import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import random 
from mpl_toolkits.mplot3d import Axes3D
import plotly.express as px
import scipy.constants


def import_coordinate_data():
    dict_x = {}
    dict_y = {}
    dict_z = {}
    atom_list = ['CA', 'C','N']
    for line in open("1yhu.pdb"):
        list = line.split()
        if list[0] =='ATOM' and list[2] in atom_list:
            dict_x[int(list[1])] = float(list[6])
            dict_y[int(list[1])] = float(list[7])
            dict_z[int(list[1])] = float(list[8])
        
    
    return dict_x, dict_y, dict_z
        


dict_x, dict_y, dict_z = import_coordinate_data()

# Print key-value pairs for dict_x
print("Dictionary X:")
for key, value in dict_x.items():
    print(f"Key: {key}, Value: {value}")

# Print key-value pairs for dict_y
print("\nDictionary Y:")
for key, value in dict_y.items():
    print(f"Key: {key}, Value: {value}")

# Print key-value pairs for dict_z
print("\nDictionary Z:")
for key, value in dict_z.items():
    print(f"Key: {key}, Value: {value}")

def import_atom_data():
    atom_dict = {}
    atom_list = ['CA', 'C','N']
    for line in open("1yhu.pdb"):
        row_list = line.split()
        if row_list[0] == 'ATOM' and row_list[2] in atom_list:
            atom_dict[int(row_list[1])] = row_list[2]
        
    return atom_dict

atom_dict = import_atom_data()

# Print atom for each index
print("\nDictionary Atom:")
for key, value in atom_dict.items():
    print(f"Key: {key}, Value: {value}")

#Fetch all in an array 
def import_index_data():
    index_list = [0]
    atom_list = ['CA', 'C', 'N']
    for line in open("1yhu.pdb"):
        row_list = line.split()
        if row_list[0] =='ATOM' and row_list[2] in atom_list: 
            index_list.append(int(row_list[1]))
    return index_list

index_list = import_index_data()


def import_x_data():
    x_list = [0]
    atom_list = ['CA', 'C', 'N']
    for line in open("1yhu.pdb"):
        row_list = line.split()
        if row_list[0] =='ATOM' and row_list[2] in atom_list: 
            x_list.append(float(row_list[6]))
    return x_list

x_list = import_x_data()

def import_y_data():
    y_list = [0]
    atom_list = ['CA', 'C', 'N']
    for line in open("1yhu.pdb"):
        row_list = line.split()
        if row_list[0] =='ATOM' and row_list[2] in atom_list: 
            y_list.append(float(row_list[7]))
    return y_list

y_list = import_y_data()

def import_z_data():
    z_list = [0]
    atom_list = ['CA', 'C', 'N']
    for line in open("1yhu.pdb"):
        row_list = line.split()
        if row_list[0] =='ATOM' and row_list[2] in atom_list: 
            z_list.append(float(row_list[8]))
    return z_list

z_list = import_z_data()

# Print index of carbon chain atom
print("\nATOMS:", index_list)
print("\nx:", x_list)
print("\ny:", y_list)
print("\nz", z_list)

def coordinates_table():
    df=pd.DataFrame()
    index_column = []
    x_column = []
    y_column = []
    z_column = []
    atom_column = []
    for key in dict_x.keys():
        index_column.append(key)
    df["index"] = pd.Series(index_column)
    for value in dict_x.values():
        x_column.append(value)
    df["x"] = pd.Series(x_column)
    for value in dict_y.values():
        y_column.append(value)
    df["y"] = pd.Series(y_column)
    for value in dict_z.values():
        z_column.append(value)
    df["z"] = pd.Series(z_column)
    for value in atom_dict.values():
        atom_column.append(value)
    df["atom"] = pd.Series(atom_column)

    return df

df = coordinates_table()

print(df)

#Build a matrix of zeros as same dimension of index_list 
def matrix():
    atom_list_length = len(dict_x)
    zeros_matrix = np.zeros((atom_list_length +1, atom_list_length +1))
    return zeros_matrix, atom_list_length

zeros_matrix, atom_list_length = matrix()

print(zeros_matrix)

#Replace first row and first column by the index_list 
def index_matrix():
    zeros_matrix[0, :] = index_list
    carbon_chain_df = np.transpose(zeros_matrix)
    carbon_chain_df[0, :] = index_list
    return carbon_chain_df

carbon_chain_df = index_matrix()
print(carbon_chain_df)

#Build diagonale matrix of one    
def carbon_chain_matrix():
    for i in range(1, atom_list_length):
        carbon_chain_df[i,i+1] = 1
    print(carbon_chain_df)
    return carbon_chain_df

carbon_chain_df = carbon_chain_matrix()

#Scatter-plot 
fig = px.scatter_3d(df, x=df["x"], y=df["y"], z=df["z"], color= df["atom"])
fig.write_html('first_figure.html', auto_open=True)

#Calculate k constant

def constante_k():
    mC = 1.9944733*(10**-26)
    mr = (mC*mC)/(mC+mC)
    n = 1320 
    lamb = (1/1320)*(10**-2)
    f = scipy.constants.c/lamb
    T = 1/f 
    k = (4*(scipy.constants.pi**2)*mr)/(T**2)
    return k, mr, lamb, f, T 

k, mr, lamb, f, T = constante_k()
 
print("constante de raideur k:",k,"en N.m-1")

#Calculate leq distance
def coordinates_matrix_x():
    matrix_x = zeros_matrix.copy()
    matrix_x[0, :] = x_list
    matrix_x = np.transpose(matrix_x)
    matrix_x[0, :] = x_list
    for i in range(1, atom_list_length):
        matrix_x[i,i+1] = 1
    return matrix_x

matrix_x = coordinates_matrix_x()

print(matrix_x)

def coordinates_matrix_y():
    matrix_y = zeros_matrix.copy()
    matrix_y[0, :] = y_list
    matrix_y = np.transpose(matrix_y)
    matrix_y[0, :] = y_list
    for i in range(1, atom_list_length):
        matrix_y[i,i+1] = 1
    return matrix_y

matrix_y = coordinates_matrix_y()

print(matrix_y)
            
def coordinates_matrix_z():
    matrix_z = zeros_matrix.copy()
    matrix_z[0, :] = z_list
    matrix_z = np.transpose(matrix_z)
    matrix_z[0, :] = z_list
    for i in range(1, atom_list_length):
        matrix_z[i,i+1] = 1
    return matrix_z

matrix_z = coordinates_matrix_z()

print(matrix_z)           

def delta_x():
    list_delta_x = []
    for i in range(1, atom_list_length):
        if matrix_x[i,i+1] == 1:
            delta_x = (matrix_x[i, 0] - matrix_x[0, i+1])**2
            list_delta_x.append(delta_x)
    return list_delta_x
list_delta_x = delta_x()

def delta_y():
    list_delta_y = []
    for i in range(1, atom_list_length):
        if matrix_y[i,i+1] == 1:
            delta_y = (matrix_y[i, 0] - matrix_y[0, i+1])**2
            list_delta_y.append(delta_y)
    return list_delta_y
list_delta_y = delta_y()

def delta_z():
    list_delta_z = []
    for i in range(1, atom_list_length):
        if matrix_z[i,i+1] == 1:
            delta_z = (matrix_z[i, 0] - matrix_z[0, i+1])**2
            list_delta_z.append(delta_z)
    return list_delta_z
list_delta_z = delta_z()

print("\ndelta x:", list_delta_x)
print("\ndelta y:", list_delta_y)
print("\ndelta z:", list_delta_z)
            
def distance():
    resolution = (3.5*(10**-10))
    for i in range(len(list_delta_x)):
        list_leq = ((list_delta_x[i] + list_delta_y[i] + list_delta_z)**(1/2))*resolution
        dim_list_leq = len(list_leq)
    return list_leq, dim_list_leq 
        
list_leq, dim_list_leq = distance()

print("\nlist leq:", list_leq)

 #Calculate potential energy            
def potential_energy():
    list_Ur = []
    for i in range(len(list_leq)):
        Ur = k*((list_leq[i])**2)
        list_Ur.append(Ur)
    return list_Ur
list_Ur = potential_energy()

def total_potential_energy():
    total_Ur = sum(list_Ur)
    return(total_Ur)

total_Ur = total_potential_energy()

print("\ntotal potential energy:", total_Ur, "N")
print(k , mr, lamb, f, T )


      
            
            
            
        


            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            



